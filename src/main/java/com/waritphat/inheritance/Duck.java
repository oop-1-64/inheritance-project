/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.inheritance;

/**
 *
 * @author domem
 */
public class Duck extends Animal {

    private int numberOfWing = 2;

    public Duck(String name, String color) {
        super(name, color, 2);
//        System.out.println("Duck Created");
    }

    public void fly() {
        System.out.println("name: " + this.name + " can flyy!!");
    }

    @Override
    public void walk() {
        System.out.println("Duck: " + name + " walk with " + numberOfLegs + " legs.");
    }

    @Override
    public void speak() {
        System.out.println("Duck: " + name + " speak > Gub Gub!!");
    }
}
