/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.inheritance;

/**
 *
 * @author domem
 */
public class Dog extends Animal {

    public Dog(String name, String color) {
        super(name, color, 4);
//        System.out.println("Dog Created");
    }

    @Override
    public void walk() {
        System.out.println("Dog: " + name + " walk with " + numberOfLegs + " legs.");
    }

    @Override
    public void speak() {
        System.out.println("Dog: "+ name + " speak > Box Box!!");
    }
}
