/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.inheritance;

/**
 *
 * @author domem
 */
public class Cat extends Animal {

    public Cat(String name, String color) {
        super(name, color, 4);
//        System.out.println("Cat Created");
    }

    @Override
    public void walk() {
        System.out.println("Cat: " + name + " walk with " + numberOfLegs + " legs.");
    }

    @Override
    public void speak() {
        System.out.println("Cat: " + name + " speak > Meow Meow!!");
    }
}
