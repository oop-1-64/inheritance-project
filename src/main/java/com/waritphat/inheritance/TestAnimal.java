/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.inheritance;

/**
 *
 * @author domem
 */
public class TestAnimal {
    public static void main(String[] args) {
        Dog dang = new Dog("Dang", "black&white");
//        dang.speak();
//        dang.walk();
        
        Dog to = new Dog("To", "orange&white");
//        to.speak();
//        to.walk();
        
        Dog mome = new Dog("Mome", "black&white");
//        mome.speak();
//        mome.walk();
        
        Dog bat = new Dog("Bat", "black&white");
//        bat.speak();
//        bat.walk();
        
        Duck som = new Duck("Som", "yellow");
//        som.speak();
//        som.walk();
//        som.fly();
        
        Duck dum = new Duck("Dum", "black");
//        dum.speak();
//        dum.walk();
//        dum.fly();
        
        Cat zero = new Cat("Zero", "orange");
//        zero.speak();
//        zero.walk();
        
        Animal[] animals = {dang, to, mome, bat, som, dum, zero};
        for (int i = 0; i < animals.length; i++)
        {
            System.out.println("Animal Created");
            animals[i].speak();
            animals[i].walk();
            if (animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
               
    }
}
